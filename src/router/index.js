import Vue from 'vue'
import Router from 'vue-router'
import { firestorePlugin } from 'vuefire'

Vue.use(firestorePlugin)

import Counter from '@/components/Counter'


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/:id?',
      name: 'Counter',
      component: Counter
    }
  ]
})
