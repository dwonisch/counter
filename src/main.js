import Vue from "vue";
import App from "./App.vue";
import router from "./router";

import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css'

import 'fontawesome'

Vue.use(Vuetify)

const opts = {}

Vue.config.productionTip = false;


new Vue({
  vuetify : new Vuetify(),
  router,
  render: h => h(App)
}).$mount("#app");