import firebase from 'firebase/compat/app'
import 'firebase/compat/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyC-7Ih9UZaXaSjlkwceltgeKc3vNUtBLx8",
    authDomain: "point-counter-d1802.firebaseapp.com",
    projectId: "point-counter-d1802",
    storageBucket: "point-counter-d1802.appspot.com",
    messagingSenderId: "509068280458",
    appId: "1:509068280458:web:56e258a715fc698f682373"
  };

// Get a Firestore instance
export const db = firebase
  .initializeApp(firebaseConfig)
  .firestore()

// if using Firebase JS SDK < 5.8.0
db.settings({ timestampsInSnapshots: true })